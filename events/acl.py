import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_weather(city, state):
    parameter = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY
    }
    resp = requests.get("http://api.openweathermap.org/geo/1.0/direct", params=parameter)
    data = resp.json()
    try:
        latitude = data[0]["lat"]
        longitude = data[0]["lon"]
    except (KeyError, IndexError):
        return None

    parameter2 = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY
    }
    resp = requests.get("https://api.openweathermap.org/data/2.5/weather", params=parameter2)
    content = resp.json()
    try:
        temp = content["main"]["temp"]
        weather_status = content["weather"][0]["description"]
    except (KeyError, IndexError):
        return None

    return {"temp": temp, "description": weather_status}


def get_picture_url(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    resp = requests.get(f"https://api.pexels.com/v1/search?query={city}+{state}", headers=headers)
    data = resp.json()
    return data["photos"][0]["url"]
